<?php 

 namespace Survey;

class Config {
    /* $config designa a la propia instancia a modo de Singleton */
    private static $config = null;
    private $__consumerKey = null;
    private $__consumerSecret = null;

    private static function instance() {
        if (!self::$config)
            self::$config = new self;

        return self::$config;
    }

    public static function consumerKey() {
        return self::instance()->_consumerKey();
    }

    public static function consumerSecret() {
        return self::instance()->_consumerSecret();
    }

    public static function consumerKeyAndroid() {
        return self::instance()->_consumerKeyAndroid();
    }

    public static function consumerSecretAndroid() {
        return self::instance()->_consumerSecretAndroid();
    }

    private function _consumerKey() {
        return $this->__consumerKey;
    }

    private function _consumerSecret() {
        return $this->__consumerSecret;
    }

    private function _consumerKeyAndroid() {
        return $this->__consumerKeyAndroid;
    }

    private function _consumerSecretAndroid() {
        return $this->__consumerSecretAndroid;
    }

    private function __construct() {
        /* Aquí cargamos la configuración de base de datos, archivo, o como queramos */
        $this->__consumerKey ="ufF4D9mb9V5KfdIiYysUG5tCN";
        $this->__consumerSecret ="gIGt6HXBO2dagh9SxAcss74qgXUuhAnj51mXEzdTh1nDBDXYF2";

        $this->__consumerKeyAndroid ="3nVuSoBZnx6U4vzUxf5w";
        $this->__consumerSecretAndroid ="Bcs59EFbbsdF6Sl9Ng71smgStWEGwXXKSjYvPVt7qys";
    }
};

?>
