<?php 
	namespace Survey;
/* Como URL de callback usaremos "oob", según la documentación debemos utilizar
   oauth_callback para mantener la compatibilidad y el valor oob para que Twitter
   no redirija a ningún lado, sino que devuelva un PIN que introduciremos en la
   aplicación para terminar la autorización. */
	class Auth{

		 private function __construct(){}

		 public static function auth($tw,$document){

			try {



			    $requestToken = $tw->oauth('oauth/request_token', ['oauth_callback' => 'oob']);

			    $url = $tw->url('oauth/authorize', [
			        'oauth_token' => $requestToken['oauth_token'],
			    ]);

			    echo "Ve a $url para autorizar la aplicación.\nLuego vuelve e inserta el PIN.\n";
			    echo "PIN:";
			    while ((!fscanf(STDIN, "%s", $pin)) || (!trim($pin)) )
			        echo "PIN: ";

			    $tw->setOauthToken($requestToken['oauth_token'], $requestToken['oauth_token_secret']);
			    $userData = $tw->oauth('oauth/access_token', [
			        'oauth_verifier' => intval($pin)
			    ]);

			    /* Visualizamos los datos de acceso del usuario */
			    var_dump($userData);
			    if($document == 'android'){
			    	UserAndroid::updateUserData($userData);
			    }else{
			   		User::updateUserData($userData);			    	
			    }
			    $tw->setOauthToken($userData['oauth_token'], $userData['oauth_token_secret']);
			    $credentials = $tw->get("account/verify_credentials");
			    /* Ya podemos hacer peticiones a Twitter si queremos */
			    //var_dump($credentials);
			    return "Credenciales Correctas";


			} catch (\Exception $e) {
			    echo($e);
			    return "error";
			}		 	

		 }

	}


 ?>