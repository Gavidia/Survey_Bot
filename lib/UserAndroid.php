<?php
namespace Survey;

class UserAndroid {
    protected static $filename = "etc/android.dat";

    protected static function getData() {
        $data = (file_exists(self::$filename))?file_get_contents(self::$filename):null;
        if (!$data)
            return array();
        $data = json_decode($data, true);
        return $data;
    }

    protected static function saveData($data) {
        file_put_contents(self::$filename, json_encode($data));
    }

    public static function getUserData($user) {

        $data = self::getData();
        return $data[$user]??null;
    }

    public static function updateUserData($data) {
        $user = $data['screen_name'];
        $currentData = self::getData();
        $currentData[$user] = $data;
        self::saveData($currentData);
    }
};

?>
